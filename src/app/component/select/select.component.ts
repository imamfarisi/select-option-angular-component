import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core'

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnChanges {

  @Input() data: any[] = []
  @Input() keyValue = ""
  @Input() keyText = ""

  @Output() change = new EventEmitter<{ value: any, text: any }>()

  @Input() class: string = ""

  ngOnChanges(): void {
    this.validates()
  }

  validates() {
    if (this.data.length < 1) throw Error('property data is empty or required')
    if (!this.keyValue || this.keyValue == "") throw Error('property keyValue is empty or required')
    if (!this.keyText || this.keyText == "") throw Error('property keyText is empty or required')
  }

  changeSelect(val: any): void {
    var index = val.target.selectedIndex;
    this.change.emit({ value: val.target.value, text: val.target[index].text })
  }

}
