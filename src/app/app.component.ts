import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app1';

  dataSelect = [
    {
      id: 1, name: 'Option 1'
    },
    {
      id: 2, name: 'Option 2'
    },
    {
      id: 3, name: 'Option 3'
    }
  ]

  dataObj = new Data()

  changeSelect(val: { value: any, text: any }) {
    if (val.value) {
      console.log(val);
      this.dataObj.id = val.value
      this.dataObj.value = val.text
    }
  }
}

class Data {
  value = ""
  id = ""
}